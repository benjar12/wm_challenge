import subprocess
import os
import sys
import time

def background_process(process_args):
    subprocess.Popen(process_args)

def generate_args(job, file_id):
    if os.environ.has_key("SPARK_HOME") is not True:
        # TODO: I'd like to push this error up to the caller. Let them handle it.
        # That said this might just be my recent golang work talking.
        raise Exception("Spark home is not set")
    spark_home = os.environ["SPARK_HOME"]
    absolute_job_path = os.path.abspath(job)
    return ["{0}/bin/spark-submit".format(spark_home), absolute_job_path, file_id]

'''
There is likely a cleaner way to implement this. Given the constraints 
this should suffice.

file_id is the uuid of the file that was uploaded
:param exec_id is the uuid of the job that was just kicked off
'''
def run_job(job_id, record_id):
    if job_id is 0:
        args = generate_args("src/jobs/import.py", record_id)
    elif job_id is 1:
        args = generate_args("src/jobs/one.py", record_id)
    elif job_id is 2:
        args = generate_args("src/jobs/two.py", record_id)
    elif job_id is 3:
        args = generate_args("src/jobs/three.py", record_id)
    elif job_id is 4:
        args = generate_args("src/jobs/four.py", record_id)
    elif job_id is 5:
        args = generate_args("src/jobs/five.py", record_id)
    elif job_id is 6:
        args =generate_args("src/jobs/six.py", record_id)
    elif job_id is 7:
        args = generate_args("src/jobs/seven.py", record_id)
    elif job_id is 8:
        args = generate_args("src/jobs/eight.py", record_id)
    elif job_id is 9:
        args = generate_args("src/jobs/nine.py", record_id)
    elif job_id is 10:
        args = generate_args("src/jobs/ten.py", record_id)
    else:
        raise Exception("Bad job id!")

    background_process(args)

if __name__ == '__main__':
    run_job(0, None)
    time.sleep(10)