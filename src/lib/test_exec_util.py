import unittest
import shutil
import os
import time
import timeout_decorator

from exec_util import *

'''
Simple test case to test the exec util, which kicks off the etl jobs.
'''
class TestExecUtil(unittest.TestCase):
    def setUp(self):
        shutil.rmtree('/tmp/wm/sample_job', ignore_errors=True)

    # TODO implement table based testing for both positive and negative cases.
    def test_spark_executable_is_set_correctly(self):
        spark_home = os.environ["SPARK_HOME"]
        should_equal = "{0}/bin/spark-submit".format(spark_home)
        resp = generate_args("foo")[0]
        self.assertEqual(resp, should_equal)

    '''
    This is more of an integration/acceptance test. I don't like mixing
    acceptance tests with unittests but it is what it is for this challenge.
    '''
    @timeout_decorator.timeout(30)
    def test_runs_sample_job(self):
        run_job(0)
        # TODO I'm sure there is a better way to detect the process is done, but
        # because were putting it in the background this was the easiest.
        time.sleep(25)
        self.assertTrue(os.path.isdir("/tmp/wm/sample_job"))
        self.assertTrue(os.path.exists("/tmp/wm/sample_job/_SUCCESS"))

if __name__ == '__main__':
    unittest.main()