import os

from pyspark.sql import SparkSession

'''
This job is a simple test that I plan to use for acceptance/unit tests,
if time permits. If it doesn't run spark it's just creates the dir and
puts _SUCCESS in there. Using some kind of fake might more make since.
'''
class SampleJob():

    def __init__(self, run_spark=True):
        self.run_spark = run_spark
        self.out_put_dir = "/tmp/wm/sample_job"


    def run(self):
        if self.run_spark:
            spark = SparkSession.builder.appName("Sample").getOrCreate()
            # TODO: have this look up folder by relative path instead of absolute hard coded.
            df = spark.read.json("/Users/benjarman/Development/wm_code_challenge/data_subset/wm_sample_50000.txt")
            df.limit(10).write.mode("overwrite").json(self.out_put_dir)
        else:
            os.makedirs(self.out_put_dir, exist_ok=True)
            fname = "{0}/__SUCCESS".format(self.out_put_dir)
            open(fname, 'a').close()


if __name__ == "__main__":
    SampleJob().run()