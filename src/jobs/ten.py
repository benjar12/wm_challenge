import sys
import json

from pyspark import SparkContext
from pyspark.sql import HiveContext

from base_job import BaseJob
from model.import_file import File
from model.job_exec import JobExec

class Ten(BaseJob):

    def job_to_run(self):

        # first we need to find which table we stored the data in
        file = self.record.file_id
        table_name = file.import_table_name

        # TODO: move some of this boilerplate into the base class.
        # Second we need to read in the data
        sc = SparkContext('local', 'Spark SQL')
        sqlc = HiveContext(sc)

        df = sqlc.read.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').load()

        df.registerTempTable("results")

        # Third we will run our query
        result_df = sqlc.sql("""
        SELECT count, state 
        FROM (
            SELECT count(distinct vendor_id) as count, state
            FROM results
            GROUP BY state
        )
        WHERE count > 0
        ORDER BY count
        LIMIT 20
        """)

        # Forth read the results and convert to json
        collected = result_df.collect()
        list_of_dists = [x.asDict() for x in collected]

        # Finally save the results
        self.record.result = json.dumps(list_of_dists)



if __name__ == '__main__':
    args = sys.argv
    job_id = args[1]
    import_job = Ten(job_id, JobExec)
    import_job.run()