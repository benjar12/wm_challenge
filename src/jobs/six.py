import sys
import json

from pyspark import SparkContext
from pyspark.sql import HiveContext

from base_job import BaseJob
from model.import_file import File
from model.job_exec import JobExec

class Six(BaseJob):

    def job_to_run(self):

        # first we need to find which table we stored the data in
        file = self.record.file_id
        table_name = file.import_table_name

        # TODO: move some of this boilerplate into the base class.
        # Second we need to read in the data
        sc = SparkContext('local', 'Spark SQL')
        sqlc = HiveContext(sc)

        df = sqlc.read.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').load()

        df.registerTempTable("results")

        # Third we will run our query
        result_df = sqlc.sql("""
            SELECT c.lab_id, c.avg+t.avg/2 as oavg, c.avg, t.avg
            FROM (
                    SELECT lab_id, AVG(cbda) as avg 
                    FROM results 
                    WHERE cbda IS NOT NULL
                    GROUP BY lab_id
                ) as c
                INNER JOIN
                (
                    SELECT lab_id, AVG(thca) as avg 
                    FROM results 
                    WHERE thca IS NOT NULL
                    GROUP BY lab_id
                ) as t ON t.lab_id = c.lab_id
            ORDER BY oavg ASC
            LIMIT 1
        """)

        collected = result_df.collect()

        first = collected[0].asDict()

        # Finally save the results
        self.record.result = json.dumps(first)

if __name__ == '__main__':
    args = sys.argv
    job_id = args[1]
    job = Six(job_id, JobExec)
    job.run()