class BaseJob():

    '''
    This method takes in the file id and class declaration as params. I chose this
    route because I want to share some business logic between file imports, and actual
    processing.
    '''
    def __init__(self, job_id, model_cls):
        self.job_id = job_id
        self.record = model_cls.get(record_id=job_id)

    '''
    This method needs to be overwritten for every job that extends this class.
    '''
    def job_to_run(self):
        raise Exception("The run method was not implemented!")

    def run(self):
        self.record.status = "started"
        self.record.save()

        try:
            self.job_to_run()
        except Exception as e:
            self.record.status = "failed"
            self.record.save()
            raise e


        self.record.status = "finished"
        self.record.save()