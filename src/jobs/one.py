import sys
import json

import pyspark.sql.functions as funcs

from pyspark import SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import lit, col
from functools import reduce
from operator import add

from base_job import BaseJob
from model.import_file import File
from model.job_exec import JobExec

class One(BaseJob):

    def job_to_run(self):
        # first we need to find which table we stored the data in
        file = self.record.file_id
        table_name = file.import_table_name

        # TODO: move some of this boilerplate into the base class.
        # Second we need to read in the data
        sc = SparkContext('local', 'Spark SQL')
        sqlc = HiveContext(sc)

        df = sqlc.read.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').load()

        counted = (df
                   .where(col("vendor_id").isNotNull())
                   .groupBy("vendor_id")
                   .agg(funcs.countDistinct("product_id"))
                   .where(col("count(DISTINCT product_id)") > 0)
                   .sort(col("count(DISTINCT product_id)").desc())
                   )

        low_row = counted.head()
        count = low_row[0][1]
        lowest_val = low_row[1]
        joined = counted.where(counted["count(DISTINCT product_id)"] == lowest_val).join(df, ["vendor_id"])
        result = joined.groupBy("vendor_id", "count(DISTINCT product_id)").agg(funcs.first(joined['vendor_id'])).head(1)[0].asDict()

        # Finally save the results
        self.record.result = json.dumps(result)

if __name__ == '__main__':
    args = sys.argv
    job_id = args[1]
    job = One(job_id, JobExec)
    job.run()