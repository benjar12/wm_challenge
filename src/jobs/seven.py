import sys
import json

from pyspark import SparkContext
from pyspark.sql import HiveContext

from base_job import BaseJob
from model.import_file import File
from model.job_exec import JobExec

class Seven(BaseJob):

    def job_to_run(self):

        # first we need to find which table we stored the data in
        file = self.record.file_id
        table_name = file.import_table_name

        # TODO: move some of this boilerplate into the base class.
        # Second we need to read in the data
        sc = SparkContext('local', 'Spark SQL')
        sqlc = HiveContext(sc)

        df = sqlc.read.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').load()

        df.registerTempTable("results")

        # Third we will run our query
        result_df = sqlc.sql("""
            SELECT dow, COUNT(dow) as count 
            FROM (SELECT DAYOFWEEK(tested_at) as dow FROM results) 
            GROUP BY dow
            ORDER BY count DESC
        """)

        collected = result_df.collect()

        first = collected[0].asDict()

        days = {1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday", 6: "Friday", 7: "Saturday"}
        map_day_to_string = lambda x: days[x]

        first["dow_string"] = map_day_to_string(first["dow"])

        # Finally save the results
        self.record.result = json.dumps(first)



if __name__ == '__main__':
    args = sys.argv
    job_id = args[1]
    job = Seven(job_id, JobExec)
    job.run()