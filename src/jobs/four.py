import sys
import json

import pyspark.sql.functions as funcs

from pyspark import SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import lit, col
from functools import reduce
from operator import add

from base_job import BaseJob
from model.import_file import File
from model.job_exec import JobExec

class Four(BaseJob):

    def get_lowest_value(self, dfi, column):
        sorted_data = dfi.where(dfi[column] > 0).sort(col(column)).select("product_id", column)
        bottom = sorted_data.head(1)[0]
        product_id = bottom[0]
        potency = bottom[1]
        return {"product_id": product_id, "potency": potency, "cannabinoid": column}

    def job_to_run(self):
        # first we need to find which table we stored the data in
        file = self.record.file_id
        table_name = file.import_table_name

        # TODO: move some of this boilerplate into the base class.
        # Second we need to read in the data
        sc = SparkContext('local', 'Spark SQL')
        sqlc = HiveContext(sc)

        df = sqlc.read.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').load()

        cannabinoids = ["cbd", "thc"]
        df = df.fillna(0, subset=cannabinoids)

        result = [self.get_lowest_value(df, x) for x in cannabinoids]

        # Finally save the results
        self.record.result = json.dumps(result)

if __name__ == '__main__':
    args = sys.argv
    job_id = args[1]
    job = Four(job_id, JobExec)
    job.run()