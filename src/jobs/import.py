import sys
import string
import random

from pyspark.sql import SparkSession
from base_job import BaseJob

from model.import_file import File

class ImportJob(BaseJob):

    def job_to_run(self):
        # This has some flaws, but the spark sql driver seems to be very picky
        # about table names. :/
        table_name = ''.join(random.choice(string.ascii_lowercase) for _ in range(25))

        spark = SparkSession.builder.appName("Import").getOrCreate()
        df = spark.read.json(self.record.file_path)
        df.write.format('jdbc').options(
            url='jdbc:postgresql://localhost/wm',
            driver='org.postgresql.Driver',
            dbtable=table_name,
            user='benjarman').save()

        self.record.import_table_name = table_name


'''
Just using a main function for a little bit of testing.
'''
if __name__ == '__main__':
    args = sys.argv
    print "Args here"
    print args
    job_id = args[1]
    import_job = ImportJob(job_id, File)
    import_job.run()