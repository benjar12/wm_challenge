import os
import uuid
import json

from flask import Flask, request, redirect, url_for
from flask import Response
from werkzeug.utils import secure_filename

from model.import_file import File
from model.job_exec import JobExec
from lib import exec_util

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "/tmp"

# TODO if there is time for getting a list of jobs by status. Do the same for imports.
@app.route('/')
def root():
    return '''
    Basic Usage:<br/></br>
    
    This app will have route to upload files.<br/>
    It will have a route to check the status of the file import.<br/>
    It will then have a route to run jobs against the data.<br/>
    Finally there will be a route to get the results.<br/><br/>
    
    Routes:<br/><br/>
    
    [PUT] /upload params: file, returns: import_id<br/>
    [GET] /import_status params: import_id returns: not_started | started | finished | failed<br/>
    [GET] /start_job<br/>
        params: import_id, job(being an int of the tasks that were assigned in this challenge)<br/>
        returns: job_id<br/>
    [GET] /result params: job_id returns either result or status<br/>
    '''

'''
This is the route used to upload files. Right now we only support one 
file. In the future we would want to add support for multi file uploads.
'''
@app.route('/upload', methods=['PUT'])
def file_upload():
    if request.method == 'PUT':
        if 'file' not in request.files:
            dist_resp = {'error': 'No file found', 'status': 400}
        file = request.files['file']
        if file:
            ext = os.path.splitext(file.filename)[-1:]
            file_id = str(uuid.uuid4())
            filename = secure_filename("{0}.{1}".format(file_id, ext))
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)

            # TODO we need to stream this to the filesystem as large files will break this.
            file.save(file_path)

            File(record_id=file_id, status="not started", file_path=file_path).save()

            exec_util.run_job(0, file_id)

            dist_resp = {'file_id': file_id, 'status': 250}

    data = json.dumps(dist_resp)
    return Response(data, status=dist_resp['status'], mimetype='application/json')


@app.route('/import_status', methods=['GET'])
def import_status():
    dist_resp = {'status': 400}
    job_id = request.args.get('job_id')
    if job_id is not None:
        f = File.get(record_id=job_id)
        if f is not None:
            dist_resp['status'] = 200
            dist_resp['import_status'] = f.status

    data = json.dumps(dist_resp)
    return Response(data, status=dist_resp['status'], mimetype='application/json')

'''
Params:
file_id will be the uuid of the upload
job_id will be between 0 - 10.
'''
@app.route('/start_job', methods=['GET'])
def start_job():
    dist_resp = {'status': 250}
    file_id = request.args.get('file_id')
    job_id = int(request.args.get('job_id'))
    record_id = str(uuid.uuid4())

    # Going to create an empty job record
    JobExec(job_number=job_id, record_id=record_id, file_id=file_id).save()

    # Next kick off that job and pass in the record id
    exec_util.run_job(job_id, record_id)

    # Finally return a 250
    dist_resp["exec_id"] = record_id

    data = json.dumps(dist_resp)
    return Response(data, status=dist_resp['status'], mimetype='application/json')
