import datetime

from peewee import *
from base import BaseModel

class File(BaseModel):
    # TODO: Figure out why null=None is not working
    import_table_name = CharField(default="")
    status = CharField()
    file_path = CharField()
    uploaded_at = DateTimeField(default=datetime.datetime.now)
