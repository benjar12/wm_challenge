from peewee import *

db = PostgresqlDatabase('wm', user='benjarman')
#db = MySQLDatabase('wm', user='root', charset='utf8mb4')

# TODO: add indexes to the models
class BaseModel(Model):
    # We are using record_id to make things generic. Also because peewee
    # does not like the id field to be anything but type of int.
    record_id = CharField(unique=True)

    class Meta:
        database = db

db.connect(True)