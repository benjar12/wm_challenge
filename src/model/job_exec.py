import datetime

from playhouse.postgres_ext import *
from base import BaseModel
from import_file import File

class JobExec(BaseModel):
    job_number = IntegerField()
    file_id = ForeignKeyField(File, to_field="record_id")
    status = CharField(default="not started")
    created_at = DateTimeField(default=datetime.datetime.now)
    result = JSONField(null = True)